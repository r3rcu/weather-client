import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/index';
import { Temperature } from '../models/Temperature';
import { API_URL } from './api';

@Injectable({
  providedIn: 'root'
})
export class TemperaturesService {

  API_URI = API_URL;

  constructor(private http: HttpClient) { }

  getTemperatures(): Observable<any> {
    return this.http.get(`${this.API_URI}Temperatures`);
  }

  saveTemperature(temperature: Temperature): Observable<any> {
    return this.http.post(`${this.API_URI}Temperatures`, temperature);
  }

  deleteTemperature(id: string|number): Observable<any> {
    return this.http.delete(`${this.API_URI}Temperatures/${id}`);
  }

  getAverage(): Observable<any> {
    return this.http.get(`${this.API_URI}Temperatures/average`);
  }

  getMedian(): Observable<any> {
    return this.http.get(`${this.API_URI}Temperatures/median`);
  }

  getMin(): Observable<any> {
    return this.http.get(`${this.API_URI}Temperatures/min`);
  }

  getMax(): Observable<any> {
    return this.http.get(`${this.API_URI}Temperatures/max`);
  }

}
