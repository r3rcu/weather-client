import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WFormComponent } from './w-form/w-form.component';
import { WStatsComponent } from './w-stats/w-stats.component';
import { WDataComponent } from './w-data/w-data.component';
import { TemperaturesService } from './services/temperatures.service';

@NgModule({
  declarations: [
    AppComponent,
    WFormComponent,
    WStatsComponent,
    WDataComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [TemperaturesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
