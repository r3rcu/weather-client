import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';

@Component({
  selector: 'app-w-stats',
  templateUrl: './w-stats.component.html',
  styleUrls: ['./w-stats.component.css']
})
export class WStatsComponent implements OnInit {

  @Input() average;
  @Input() median;
  @Input() max;
  @Input() min;

  constructor() { }

  ngOnInit() {
  }

}
