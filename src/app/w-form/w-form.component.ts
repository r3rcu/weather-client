import { Component, OnInit } from '@angular/core';
import { TemperaturesService } from '../services/temperatures.service';
import { Temperature } from '../models/Temperature';

@Component({
  selector: 'app-w-form',
  templateUrl: './w-form.component.html',
  styleUrls: ['./w-form.component.css']
})
export class WFormComponent implements OnInit {

  average: object;
  median: object;
  max: object;
  min: object;

  temperatures: object = [];

  new_temperature: Temperature = {
    value: 25,
    date: new Date(),
  };

  constructor(private services: TemperaturesService) { }

  ngOnInit() {
    this.getStats();
    this.getList();
  }

  getStats(): void {
    this.services.getAverage().subscribe(
      res => {
        this.average = res;
      },
      err => console.log(err)
    );

    this.services.getMedian().subscribe(
      res => {
        this.median = res;
      },
      err => console.log(err)
    );

    this.services.getMin().subscribe(
      res => {
        this.min = res;
      },
      err => console.log(err)
    );

    this.services.getMax().subscribe(
      res => {
        this.max = res;
      },
      err => console.log(err)
    );
  }

  getList(): void {
    this.services.getTemperatures().subscribe(
      res => {
        this.temperatures = res;
      },
      err => console.log(err)
    );
  }

  save(): void {
    this.services.saveTemperature(this.new_temperature).subscribe(
      res => {
        console.log(res);
        this.getStats();
        this.getList();
        //this.temperatures.push(this.new_temperature);
      },
      err => console.log(err)
    );
  }

  delete($event): void {
    this.services.deleteTemperature($event.id).subscribe(
      res => {
        this.getStats();
        this.getList();
      },
      err => console.log(err)
    );
  }

}
