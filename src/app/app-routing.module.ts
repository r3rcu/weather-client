import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { WDataComponent } from './w-data/w-data.component';
import { WFormComponent } from './w-form/w-form.component';

const routes: Routes = [
  {path: '', component: WFormComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
