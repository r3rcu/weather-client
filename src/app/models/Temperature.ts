export interface Temperature {
  id?: number;
  date: Date;
  value: number;
}
