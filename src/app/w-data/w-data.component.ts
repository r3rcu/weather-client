import { Component, OnInit } from '@angular/core';
import { Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-w-data',
  templateUrl: './w-data.component.html',
  styleUrls: ['./w-data.component.css']
})
export class WDataComponent implements OnInit {

  @Input() temperatures;
  @Output() deleting = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

}
