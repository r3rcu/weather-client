1. Inside this folder execute "npm install"
2. If the Rest API is running by a different address than localhost:3000, you can edit the url in "src/app/services/api.ts"
3. To run the development server execute "ng serve"
